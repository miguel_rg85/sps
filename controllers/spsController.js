'use strict'

const express   = require('express');
const router    = express.Router();

module.exports = {
  getMessage: ( req, res ) => {
    res.send({success: true, message: 'Hola Mundo'})
  },

  setMessage: ( req, res ) => {
    if( !req.body.message )
      res.send({success: false, message: 'No se ha recibido algún parametro'})
    else
      res.send({success: true, message: req.body.message});
  }

}
