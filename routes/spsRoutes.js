const express       = require('express');
const router        = express.Router();

const spsController = require('../controllers/spsController');

router.get('/getMessage', spsController.getMessage);
router.post('/setMessage', spsController.setMessage);


module.exports = router;
