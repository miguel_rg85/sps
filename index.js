const express       = require('express');
const app           = express();
const logger        = require('morgan');
const port          = process.env.PORT || 8090;
const bodyParser    = require('body-parser');
const cookieSession = require('cookie-session');


// Configurar archivo de rutas base
var sps_routes      = require('./routes/spsRoutes');


// Logger para desarrollo
app.use(logger('dev'));


// Middleware de body-parser para JSON
app.use(bodyParser.json())
app.use(bodyParser.urlencoded( { extended: false } ));


// Manejo de Cookies
app.use(cookieSession({
  name: 'session',
  keys: ['llave-12', 'llave-21'],
  maxAge: 1 * 60 * 60 * 1000
}));


// Headers, CORS
app.use( (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method',
  );
  res.header(
    'Access-Control-Allow-Methods',
    'GET, PUT, POST, DELETE'
  );
  res.header(
    'Allow',
    'GET, PUT, POST, DELETE, OPTIONS'
  );
  next();
});


// Leer rutas base
app.use('/api/sps/helloworld/v1', sps_routes);


// Manejo de errores
app.use( (err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render de la pagina de error
  res.status(err.status || 500);
  res.send(err);
});


// Iniciar server
app.listen(port, () => console.log(`Server listening on ${port} ...`));
